// import { createApp } from 'vue'
// import Vue from 'vue';
// import App from './App.vue';
// import router from './router'; // Import the router configuration

// Vue.config.productionTip = false;

// new Vue({
//   router, // Attach the router to the Vue instance
//   render: h => h(App),
// }).$mount('#app');

// createApp(App).mount('#app')

import { createApp } from 'vue'
import App from './App.vue'

import {createRouter, createWebHistory} from 'vue-router'  
import HomePage from './components/HomePage.vue';
import GetProducts from './components/GetProducts.vue';
import BillingPage from './components/BillingPage.vue';
import CreateProducts from './components/CreateProducts.vue';

const routes = [
    { path: '/', component: HomePage },
    { path: '/more', component: GetProducts },
    { path: '/bill', component: BillingPage, props: (route) => ({ id: route.query.id }) },
    { path: '/products', component: CreateProducts },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})
createApp(App).use(router).mount('#app')
