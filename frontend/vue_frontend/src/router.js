
// import Vue from 'vue';
import VueRouter from 'vue-router';
import HomePage from './components/HomePage.vue';
import GetProducts from './components/GetProducts.vue';

// Vue.use(VueRouter);

const routes = [
  { path: '/', component: HomePage },
  { path: '/more', component: GetProducts },
];

const router = new VueRouter({
  routes,
});

export default router;
