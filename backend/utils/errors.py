from sanic.exceptions import SanicException


def set_defaults(code, message):
    """Decorator used to set default code and message in exceptions for :class:`BaseCustomException`."""

    def class_decorator(cls):
        cls.DEFAULT_STATUS_CODE = code
        cls.DEFAULT_MESSAGE = message
        return cls

    return class_decorator


class BaseCustomException(SanicException):
    """Base class that handles errors and return a response error object with given status code.
       Supports raising custom error message and status code as well.

       @Note: Currently there's a common code for both showing to the API user and response.status_code.
              Keep error code appropriate within the supported status code in response.

       For eg.
        `raise Unauthorized` will return {'code': 401, 'message': 'Bad Data'}
       Custom code and message on the fly is also supported,
       `raise Unauthorized('Some custom message', 422)` will return {'code': 422, 'message': 'Some custom message'}
    """
    DEFAULT_MESSAGE = 'Bad Data'
    DEFAULT_STATUS_CODE = 400

    def __init__(self, custom_message=None, custom_status_code=None, error_dict=None):
        # Call the base class constructor with the parameters it needs
        message = custom_message if custom_message else self.DEFAULT_MESSAGE
        status_code = custom_status_code if custom_status_code else self.DEFAULT_STATUS_CODE
        super().__init__(message, status_code)

        self.errors = {'code': status_code, 'message': message}

        if error_dict:
            self.errors['errors'] = error_dict


@set_defaults(401, "User is not logged in")
class Unauthorized(BaseCustomException):
    pass


@set_defaults(403, "Access Denied")
class Forbidden(BaseCustomException):
    pass


@set_defaults(400, "Invalid request")
class InvalidRequest(BaseCustomException):
    pass


@set_defaults(404, "data not found")
class NotFound(BaseCustomException):
    pass


@set_defaults(400, "x-app-id or x-app-token missing in request headers")
class InvalidHeader(BaseCustomException):
    pass


@set_defaults(400, "x-app-id or x-app-token or x-auth-token missing in request headers")
class NotAllowed(BaseCustomException):
    pass


@set_defaults(503, "Service is temporarily unable to handle the request")
class Unavailable(BaseCustomException):
    pass