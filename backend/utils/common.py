import ujson
from utils.errors import Unauthorized
from datetime import datetime
from bson import ObjectId
from typing import Any

class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

def convert_str_to_bool(value):
    try:
        return value.lower() in ("yes", "true", "t", "1")
    except Exception as e:
        return False

def validate_x_user_data(request):
    try:
        user_data = ujson.loads(request.headers.get("x-user-data"))
        _ = user_data["uid"]
    except:
        raise Unauthorized
    return {'username': user_data.get('username'), 'user_id': user_data.get('_id')}

def json_dump_serializer(data: Any):
    if isinstance(data, (datetime,)):
        return data.isoformat()

    if isinstance(data, (ObjectId,)):
        return str(data)

    return str(data)

class UjsonDump:
    @staticmethod
    def dumps(data):
        return ujson.dumps(data, default=json_dump_serializer)