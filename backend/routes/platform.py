from product.urls.platform import product_pltm_apis

class PlatformRouters:
    @staticmethod
    def get() -> list:
        return [
            product_pltm_apis,
        ]
