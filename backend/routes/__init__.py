class Routes:
    @staticmethod
    def get(server_type: str) -> list:
        if server_type == "platform":
            from routes.platform import PlatformRouters
            return PlatformRouters.get()
        # elif server_type == "administrator"
        # add server types as needed
        return []

from sanic import Blueprint
from importlib import import_module

class RoutesFactory:
    ROUTERS = {
        "platform": "routes.platform.PlatformRouters",
    }

    @staticmethod
    def import_router(router_class):
        router = getattr(import_module(router_class), "get")
        return router()

    @classmethod
    def get(cls, server_type: str) -> Blueprint:
        if server_type in ["local", "test"]:
            router_blueprints = Blueprint.group()
            for key, value in cls.ROUTERS.items():
                router = cls.import_router(value)
                router_blueprints.blueprint(router, url_prefix=key)
            return router_blueprints

        router_class = cls.ROUTERS.get(server_type)
        if router_class:
            return cls.import_router(router_class)

        return None