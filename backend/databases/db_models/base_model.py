from marshmallow import Schema, ValidationError, fields
from motor.motor_asyncio import AsyncIOMotorClient
from pymongo import ReadPreference, UpdateOne
from utils.common import Singleton

class IndexModel(object):
    def __init__(
        self, index_keys, name, unique, background, sparse, expireAfterSeconds=None
    ):
        self.keys = index_keys
        self.name = name
        self.unique = unique
        self.background = background
        self.sparse = sparse
        self.expireAfterSeconds = expireAfterSeconds


class GetDBConnectionV2(metaclass=Singleton):
    """
    Initialise this first in your app
    """

    DB_CONN = {}

    def initialise_db_conn(self, io_loop, db_config, app_name=None):
        for db_name in db_config:
            self.DB_CONN[db_name] = self.connect_db(
                io_loop, db_config[db_name], db_name, app_name
            )

    @staticmethod
    def connect_db(io_loop, config, db_name, app_name):
        host = config.get("host")
        if not host:
            raise AttributeError("Invalid/Missing Host")
        if app_name:
            db_client = AsyncIOMotorClient(
                host,
                io_loop=io_loop,
                appname=app_name,
                read_preference=config.get(
                    "read_preference", ReadPreference.SECONDARY_PREFERRED
                ),
            )
        else:
            db_client = AsyncIOMotorClient(
                host,
                io_loop=io_loop,
                read_preference=config.get(
                    "read_preference", ReadPreference.SECONDARY_PREFERRED
                ),
            )
        return db_client.get_database()

    def get_db_conn(self, db):
        if self.DB_CONN.get(db) is None:
            raise NotImplementedError("Database {0} not initialised".format(db))
        return self.DB_CONN.get(db)