from marshmallow import Schema, fields, EXCLUDE, post_load

class OrderSchema(Schema):
    product_uid = fields.Int()
    product_quantity = fields.Int()

class BillingModelSchema(Schema):
    user_name = fields.Str()
    order = fields.List(fields.Nested(OrderSchema))
    total_amount = fields.Float()