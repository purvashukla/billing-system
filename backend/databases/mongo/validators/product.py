from marshmallow import Schema, fields, EXCLUDE, validates_schema, ValidationError

class ProductModelSchema(Schema):
    _id = fields.Str()
    uid = fields.Int()
    name = fields.Str(required=True)
    company_id = fields.Int(required=True)
    price = fields.Float(required=True)
    discount = fields.Int(required=True)
    discounted_price = fields.Float(required=False)

    @validates_schema
    def validate_discount(self, data,  *args, **kwargs):
        if data.get("discount", 1) < 1:
            raise ValidationError("Discount percent must be greater than 0.")


class ProductUpdateSchema(Schema):
    uid = fields.Int()