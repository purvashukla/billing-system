import re
from typing import Dict
from utils.errors import NotFound
from motor.motor_asyncio import AsyncIOMotorClient
from pymongo import ReadPreference
from asyncio.events import AbstractEventLoop
from motor.motor_asyncio import AsyncIOMotorClientSession
from contextlib import asynccontextmanager
from typing import AsyncGenerator


class _DbClient:
    DATABASES = {}
    mongo_url = r"^(mongodb:(?:\/{2})?)((\w+?):(\w+?)@|:?@?)(\S+?):(\d+)\/(\S+?)(\?replicaSet=(\S+?))?$"

    def __init__(
        self,
        databases: Dict[str, Dict[str, str]],
        loop: AbstractEventLoop,
        app_name: str,
    ) -> None:
        regex = re.compile(self.mongo_url)

        for db_name, db_config in databases.items():
            assert isinstance(db_name, str), "Not a valid database name"

            connection = db_config.get("host")
            # assert regex.search(connection), "Not a valid connection string."

            if self.DATABASES.get(db_name) is None:
                self.DATABASES[db_name] = AsyncIOMotorClient(
                    connection,
                    appname=app_name,
                    read_preference=db_config.get(
                        "read_preference", ReadPreference.SECONDARY_PREFERRED
                    ),
                    io_loop=loop,
                )[db_name]


def connection(config: dict) -> None:
    """
    #### Params:
    - databases: {'foo': {'host': 'mongodb://localhost:27017/foo'}}
    - loop: event loop
    - app_name
    """

    # one time database connection set up
    if config.get("loop"):
        _DbClient(config.get("databases"), config.get("loop"), config.get("app_name"))
    else:
        _DbClient(config.get("databases"), config.get("app_name"))


def get_db_connection(db_name):
    
    if _DbClient.DATABASES.get(db_name) is None : 
        raise NotFound( f"Connection for {db_name} DB not initialized.")

    return _DbClient.DATABASES[db_name]

@asynccontextmanager
async def transaction(db_name) -> AsyncGenerator[AsyncIOMotorClientSession, None]:
    client = get_db_connection(db_name).client
    async with await client.start_session() as session:
        yield session