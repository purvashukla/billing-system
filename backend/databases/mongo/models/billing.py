from databases.mongo.models.base_model import MongoModel
from pymongo import ASCENDING, DESCENDING, IndexModel
from databases.mongo.validators.billing import (
    BillingModelSchema
)

class Billing(MongoModel):
    DB: str = "starjet"
    COLLECTION: str = "billing"
    SCHEMA = BillingModelSchema