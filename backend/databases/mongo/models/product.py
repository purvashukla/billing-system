from databases.mongo.models.base_model import MongoModel
from pymongo import ASCENDING, DESCENDING, IndexModel
from databases.mongo.validators.product import (
    ProductModelSchema
)

class Product(MongoModel):
    DB: str = "starjet"
    COLLECTION: str = "product"
    SCHEMA = ProductModelSchema
    INDEX_LIST = [
        IndexModel([("uid", ASCENDING)], background=True, name="uid_1", unique=True),
    ]
