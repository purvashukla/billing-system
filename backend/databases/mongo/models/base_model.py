from typing import Any, List, Tuple

from motor.motor_asyncio import AsyncIOMotorCollection
from pymongo.collection import Collection
from databases.mongo import _DbClient
from pymongo import ReadPreference
# from databases.mongo.validators import BaseValidationSchema


class Indexes:
    """
    Global Index Mapping
    """

    _INDEX_LIST = {}

    @classmethod
    def add_index(cls, db: str, collection: str, indexes: list):
        cls._INDEX_LIST[(db, collection)] = indexes

    @classmethod
    async def create_indexes(cls, collections: List[str] = None):
        for db_collection, index_list in cls._INDEX_LIST.items():
            db, collection = db_collection

            if collections and collection not in collections:
                continue

            db_model: Collection = _DbClient.DATABASES[db][collection]
            index_creation_response = await db_model.create_indexes(index_list)

class Base(type):
    def __new__(cls, name, bases, body, *args, **kwargs) -> Any:
        if name not in ("MongoModel", "IndicatorModel"):
            DB = body.get("DB")
            COLLECTION = body.get("COLLECTION")
            INDEX_LIST = body.get("INDEX_LIST") or []

            assert isinstance(DB, str), f"Not a valid DB name for {name} model."
            assert isinstance(
                COLLECTION, str
            ), f"Not a valid COLLECTION name for {name} model."

            if INDEX_LIST:

                # support for readonly models
                assert isinstance(
                    INDEX_LIST, (list, tuple)
                ), f"Not a valid list of indexes for {name} model."

                Indexes.add_index(DB, COLLECTION, INDEX_LIST)

        return super().__new__(cls, name, bases, body, *args, **kwargs)


class MongoModel(metaclass=Base):
    """
    connect method on model return motor/similar-to-pymongo collection
    """

    DB: str
    COLLECTION: str
    INDEX_LIST: tuple
    # SCHEMA: BaseValidationSchema

    @classmethod
    def connect(cls, primary=False) -> AsyncIOMotorCollection:
        
        if _DbClient.DATABASES.get(cls.DB) is None :
            raise Exception(f"Connection for {cls.DB} DB not initialized.")

        collection = _DbClient.DATABASES[cls.DB][cls.COLLECTION]
        if primary:
            return collection.with_options(read_preference=ReadPreference.PRIMARY)
        return collection

    @classmethod
    async def get_total_count_and_documents(
        cls, query: dict, sort, skip=0, limit=12, dump = False
    ) -> Tuple[int, List[dict]]:
        count = await cls.connect().count_documents(query)
        if not count:
            return count, []
        cursor = cls.connect().find(query).sort(sort).limit(limit).skip(skip)
        if dump:
            return count, [cls.serialize(i) async for i in cursor]
        return count, [i async for i in cursor]

    @classmethod
    async def insert_and_get_one_document(cls, data: dict) -> dict:
        # data = cls.SCHEMA().load(data)
        response = await cls.connect().insert_one(data)
        data.update({"_id": response.inserted_id})
        return data

    @classmethod
    async def get_latest_uid(cls):
        """
        Returns the next uid to save record on.
        """
        records = cls.connect(primary=True).find({}).sort("uid", -1).limit(1)
        async for record in records:
            return record.get("uid") + 1
        return 1