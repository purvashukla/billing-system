from databases.mongo.validators.product import (
    ProductModelSchema
)
from databases.mongo.models.product import Product as ProductModel
from utils.common import validate_x_user_data
from pymongo.errors import DuplicateKeyError
from utils.errors import InvalidRequest
from marshmallow import ValidationError

async def validate_product_post_request(request, company_id):

    _user = validate_x_user_data(request)
    request_data = request.json or {}
    request_data["company_id"] = company_id
    try:
        product_data = ProductModelSchema().load(request_data)
    except DuplicateKeyError:
        raise InvalidRequest("Product already exists!")
    except ValidationError as err:
        raise InvalidRequest(err.messages)
    return product_data, _user

async def validate_product_put_request(request, company_id, uid):

    _user = validate_x_user_data(request)
    request_data = request.json or {}
    request_data["company_id"] = company_id
    request_data["uid"] = uid
    try:
        product_data = ProductModelSchema().load(request_data)
        product = await ProductModel.connect().find_one({"uid":product_data.get("uid")})
        if not product:
            raise InvalidRequest("Product not found.")
    except ValidationError as err:
        raise InvalidRequest(err.messages)
    return product_data, _user