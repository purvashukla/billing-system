from databases.mongo.models.product import Product as ProductModel
from databases.mongo.validators.product import (
    ProductUpdateSchema
)
from utils.errors import InvalidRequest

class ProductHelper:

    async def save(self, request_data):
        request_data["uid"] = await ProductModel.get_latest_uid()
        request_data["discounted_price"] = request_data["price"] * (1 - request_data["discount"] / 100)
        product = await ProductModel.insert_and_get_one_document(request_data)
        return product

    async def update(self, data):
        query = {"uid": data.get("uid")}
        try:
            data["discounted_price"] = data["price"] * (1 - data["discount"] / 100)
            product = await ProductModel.connect().update_one(query, {"$set": data})
        except Exception as e:
            raise InvalidRequest("Update Failure")


async def get_products(data):
    
    response_data = []
    query = {"company_id": data["company_id"]}
    products = ProductModel.connect().find(query)
    async for product in products:
        response_data.append(product)
        product.pop("_id")
    return response_data

async def delete_products(query):
    
    query = ProductUpdateSchema().load(query)
    await ProductModel.connect().delete_one(query)