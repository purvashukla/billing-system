from sanic.views import HTTPMethodView
from sanic import response
from product.helpers.validate_details import validate_product_post_request, validate_product_put_request
from product.helpers.product import ProductHelper, get_products, delete_products
import ujson
from utils.common import UjsonDump

class ProductsCreate(HTTPMethodView):
    async def post(self, request, company_id):

        request_data, _user = await validate_product_post_request(request, company_id)
        product_helper = ProductHelper()
        await product_helper.save(request_data)
        return response.json({"success": True})

    async def get(self, request, company_id):

        data = request.args
        data["company_id"] = company_id
        products = await get_products(data)
        resp = {"items": products}
        return response.json(resp)


class ProductUpdateDelete(HTTPMethodView):
    async def put(self, request, company_id, uid):

        request_data, _user = await validate_product_put_request(request, company_id, uid)
        product_helper = ProductHelper()
        await product_helper.update(request_data)
        return response.json({"success": True})

    async def delete(self, request, company_id, uid):

        delete_query = {"uid": uid}
        await delete_products(delete_query)
        return response.json({"success":True})