from sanic import Blueprint

from product.views import ProductsCreate, ProductUpdateDelete

product_pltm_apis = Blueprint(
    'pltm_products', url_prefix='/v1.0/company/<company_id:int>/')

# list, create
product_pltm_apis.add_route(ProductsCreate.as_view(), 'products/')

# retrive, update, delete
product_pltm_apis.add_route(
    ProductUpdateDelete.as_view(), 'product/<uid:int>/')

# # validate uniqueness
# product_pltm_apis_v1_0.add_route(ProductValidationView.as_view(), 'products/validation/')