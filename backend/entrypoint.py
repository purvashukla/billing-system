from starjet.config import settings
import asyncio

class Main:
    _EVENT_LOOP = asyncio.get_event_loop()

    @classmethod
    def get_running_event_loop(cls) -> asyncio.events.AbstractEventLoop:
        return cls._EVENT_LOOP

if __name__ == "__main__":
    if settings.MODE == "server":
        from run import exec
        exec()

    # elif settings.MODE == "consumer":
    #     import consumers.start_consumer
    # elif settings.MODE == "cron":
    #     import cron.crons
