from starjet import Starjet
from starjet.config import settings

app = Starjet.create_app()


def exec():
    app.run(
        host="0.0.0.0",
        port=int(settings.PORT),
        auto_reload=False,
        debug=settings.DEBUG,
        access_log=False,
        single_process=True,
    )
