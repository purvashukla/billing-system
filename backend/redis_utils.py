from urllib.parse import urlparse, parse_qs
import redis
import ujson
import redis.asyncio as aioredis
from redis.cluster import RedisCluster
from utils.common import Singleton, json_dump_serializer


class ConnectRedis(metaclass=Singleton):
    CACHES = {}

    def __init__(self, cache_config):
        self.initialise_redis_connections(cache_config)
        self.redis_connections = {}
        self.sync_redis_connections = {}

    @classmethod
    def clear_connections(cls):
        if cls in cls._instances:
            cls._instances.pop(cls)

    @staticmethod
    def initialise_redis_connections(cache_config):
        for key, value in cache_config.items():
            if not isinstance(value, str):
                continue
            url = urlparse(value)
            query = parse_qs(url.query)
            ConnectRedis.CACHES[key] = {
                "url": str(url.scheme) + "://" + str(url.netloc) + str(url.path),
                "port": url.port,
                "host": url.hostname,
            }

            locs = url.netloc.split(".", 1)
            alias = key
            if len(locs) > 1:
                alias = locs[0]

            ConnectRedis.CACHES[key]["alias"] = alias

            if url.path:
                ConnectRedis.CACHES[key]["db"] = str(url.path).strip("/")

            if query.get("decode_responses"):
                ConnectRedis.CACHES[key]["decode_responses"] = (
                    query.get("decode_responses")[0] == "true"
                )

            if query.get("sharded_db"):
                ConnectRedis.CACHES[key]["sharded_db"] = (
                    query.get("sharded_db")[0] == "true"
                )
            if query.get("ssl"):
                ConnectRedis.CACHES[key]["ssl"] = query.get("ssl")[0] == "true"

            if query.get("port"):
                ConnectRedis.CACHES[key]["port"] = query.get("port")

    @staticmethod
    def get_connection_settings(conn_name):
        if not ConnectRedis.CACHES.get(conn_name):
            raise ConnectionError("Redis Config {0} not found".format(conn_name))
        redis_url = ConnectRedis.CACHES[conn_name].get("url")
        redis_db = ConnectRedis.CACHES[conn_name].get("db")
        decode_responses = ConnectRedis.CACHES[conn_name].get("decode_responses", True)
        sharded_db = ConnectRedis.CACHES[conn_name].get("sharded_db", False)
        host = ConnectRedis.CACHES[conn_name].get("host")
        ssl = ConnectRedis.CACHES[conn_name].get("ssl", False)
        port = ConnectRedis.CACHES[conn_name].get("port")
        return redis_url, redis_db, decode_responses, sharded_db, host, ssl, port

    def sync_connect(self, conn_name):
        if self.sync_redis_connections.get(conn_name):
            return self.sync_redis_connections.get(conn_name)

        (
            redis_url,
            redis_db,
            decode_responses,
            sharded_db,
            host,
            ssl,
            port,
        ) = self.get_connection_settings(conn_name)
        if sharded_db:
            if ssl:
                redis_client = RedisCluster(
                    host=host,
                    port=port,
                    ssl=ssl,
                    decode_responses=decode_responses,
                    require_full_coverage=False,
                )
            else:
                redis_client = RedisCluster(
                    host=host,
                    port=port,
                    decode_responses=decode_responses,
                    require_full_coverage=False,
                )
        else:
            redis_client = redis.StrictRedis(
                host=host,
                port=port,
                ssl=ssl,
                db=redis_db,
                decode_responses=decode_responses,
            )
        self.sync_redis_connections[conn_name] = redis_client
        return redis_client

    async def connect(self, conn_name, sync_conn=False, **kwargs):
        if sync_conn:
            return self.sync_connect(conn_name)

        if self.redis_connections.get(conn_name):
            return self.redis_connections.get(conn_name)

        (
            redis_url,
            redis_db,
            decode_responses,
            sharded_db,
            host,
            ssl,
            port,
        ) = self.get_connection_settings(conn_name)

        if sharded_db:
            redis_client = await aioredis.ConnectionPool.from_url(url=redis_url)
        else:
            redis_client = await aioredis.from_url(url=redis_url, db=redis_db)
        self.redis_connections[conn_name] = redis_client
        return redis_client


class PlatformCache(object):
    """
    common func to serialize and cache operation
    """

    def __init__(self, conn_name, sync_conn=True):

        if not ConnectRedis.CACHES.get(conn_name):
            raise ConnectionError("Redis Config {0} not found".format(conn_name))
        self.conn_name = conn_name
        self.read_conn_name = conn_name
        self.sync_conn = sync_conn
        # setting for orbis_cache as cluster is not supported in aioredis
        _, _, _, is_sharded, host, ssl, port = ConnectRedis.get_connection_settings(
            self.conn_name
        )
        if is_sharded:
            self.sync_conn = True
        # get read_only
        if ConnectRedis.CACHES.get("{}_ro".format(conn_name)):
            self.read_conn_name = "{}_ro".format(conn_name)
        else:
            self.read_conn_name = conn_name

    async def connection(self, rw=True, **kwargs):
        if rw:
            return await ConnectRedis().connect(
                self.conn_name, self.sync_conn, **kwargs
            )
        else:
            return await ConnectRedis().connect(
                self.read_conn_name, self.sync_conn, **kwargs
            )

    async def cache_list(self, data, cache_key, expire_time, not_allow_empty=False):
        if data or not_allow_empty:
            try:
                redis_conn = await self.connection()
                try:
                    value = ujson.dumps(data, default=json_dump_serializer)
                except TypeError:
                    value = ujson.dumps(data)

                if self.sync_conn:
                    redis_conn.setex(
                        cache_key,
                        expire_time,
                        ujson.dumps(data, default=json_dump_serializer),
                    )
                else:
                    await redis_conn.setex(
                        cache_key,
                        expire_time,
                        ujson.dumps(data, default=json_dump_serializer),
                    )
            except aioredis.ConnectionError as e:
                ConnectRedis.CACHES = {}

    async def get_hash_data(self, cache_key):
        redis_conn = await self.connection(rw=False)
        data = {}
        try:
            if self.sync_conn:
                data = redis_conn.hgetall(cache_key)
            else:
                data = await redis_conn.hgetall(cache_key)
                if data:
                    data = ujson.loads(ujson.dumps(data, default=json_dump_serializer))

        except aioredis.ConnectionError as e:
            ConnectRedis.CACHES = {}
        return data


    async def get_cached_list(self, cache_key):
        redis_conn = await self.connection(rw=False)
        data = {}
        try:
            if self.sync_conn:
                data = redis_conn.get(cache_key)
            else:
                data = await redis_conn.get(cache_key)
            if data:
                data = ujson.loads(data)

        except aioredis.ConnectionError as e:
            ConnectRedis.CACHES = {}
        return data



    async def delete_cached_list(self, cache_key):
        if cache_key:
            try:
                redis_conn = await self.connection(rw=False)
                if self.sync_conn:
                    keys = redis_conn.keys(cache_key)
                else:
                    keys = await redis_conn.keys(cache_key)
                if keys:
                    redis_conn = await self.connection()
                    if self.sync_conn:
                        redis_conn.delete(*keys)
                    else:
                        await redis_conn.unlink(*keys)
            except aioredis.ConnectionError as e:
                ConnectRedis.CACHES = {}

    async def fetch_data_from_pipeline(
        self, redis_key_fmt, identifiers, application=None
    ):
        redis_conn = await self.connection(rw=False)
        results = []
        try:
            pipeline = redis_conn.pipeline()
            for identifier in identifiers:
                if application:
                    pipeline.get(redis_key_fmt.format(application, identifier))
                else:
                    pipeline.get(redis_key_fmt.format(identifier))

            if self.sync_conn:
                results = pipeline.execute()
            else:
                results = await pipeline.execute()
        except aioredis.ConnectionError as e:
            ConnectRedis.CACHES = {}

        return results

    async def add_data_to_pipeline(self, cache_map, ttl=3600):
        results = []
        if cache_map:
            try:
                redis_conn = await self.connection()
                pipeline = redis_conn.pipeline()
                for k, v in cache_map.items():
                    if ttl == -1:
                        pipeline.set(k, v)
                    else:
                        pipeline.setex(k, ttl, v)
                if self.sync_conn:
                    results = pipeline.execute()
                else:
                    results = await pipeline.execute()
            except aioredis.ConnectionError as e:
                ConnectRedis.CACHES = {}
        return results

    async def scan_and_fetch_data(self, cache_key, count=100):
        if cache_key:
            try:
                redis_conn = await self.connection()
                pipeline = redis_conn.pipeline()

                if self.sync_conn:
                    keys = redis_conn.scan_iter(cache_key, count=count)
                    for key in keys:
                        pipeline.get(key)
                    return pipeline.execute()
                else:
                    keys = redis_conn.iscan(match=cache_key, count=count)
                    async for key in keys:
                        pipeline.get(key)
                    return await pipeline.execute()
            except aioredis.ConnectionError as e:
                ConnectRedis.CACHES = {}

    async def clear_data(self, cache_key):
        if cache_key:
            try:
                redis_conn = await self.connection()
                pipeline = redis_conn.pipeline()

                if self.sync_conn:
                    keys = redis_conn.scan_iter(cache_key)
                    for key in keys:
                        pipeline.delete(key)
                    pipeline.execute()
                else:
                    keys = redis_conn.iscan(match=cache_key)
                    async for key in keys:
                        pipeline.delete(key)
                    await pipeline.execute()
            except aioredis.ConnectionError as e:
                ConnectRedis.CACHES = {}

    async def get_cached_list_with_hget(self, cache_key, hash_key, fetch_all=False):
        redis_conn = await self.connection(rw=False)
        try:
            if self.sync_conn:
                data = (
                    redis_conn.hgetall(cache_key)
                    if fetch_all
                    else redis_conn.hget(cache_key, hash_key)
                )
            else:
                data = (
                    await redis_conn.hgetall(cache_key)
                    if fetch_all
                    else await redis_conn.hget(cache_key, hash_key)
                )
            if data and fetch_all:
                result = [ujson.loads(item) for item in data.values()]
                return {"config": result}
            elif data:
                return ujson.loads(data)
        except aioredis.ConnectionError as e:
            ConnectRedis.CACHES = {}
        return data

    async def cache_list_with_hset(
        self, cache_key, hash_key, data, ttl=3600, not_allow_empty=False
    ):
        if data or not_allow_empty:
            try:
                redis_conn = await self.connection()
                try:
                    value = ujson.dumps(data, default=json_dump_serializer)
                except TypeError:
                    value = ujson.dumps(data)

                if self.sync_conn:
                    redis_conn.hset(
                        cache_key,
                        hash_key,
                        ujson.dumps(data, default=json_dump_serializer),
                    )
                    if ttl != -1:
                        redis_conn.expire(cache_key, ttl)
                else:
                    await redis_conn.hset(
                        cache_key,
                        hash_key,
                        ujson.dumps(data, default=json_dump_serializer),
                    )
                    if ttl != -1:
                        await redis_conn.expire(cache_key, ttl)
            except aioredis.ConnectionError as e:
                ConnectRedis.CACHES = {}

    async def delete_cached_list_with_hdel(self, cache_key, hash_key):
        if cache_key:
            try:
                redis_conn = await self.connection()
                if self.sync_conn:
                    redis_conn.hdel(cache_key, hash_key)
                else:
                    await redis_conn.hdel(cache_key, hash_key)
            except aioredis.ConnectionError as err:
                ConnectRedis.CACHES = {}
