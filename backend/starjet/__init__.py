import asyncio
import logging
from sanic import Blueprint, Sanic, response
from sanic.exceptions import NotFound, RequestTimeout
from sanic.request import Request
from databases import initialize_mongo_connection
from databases.db_models.base_model import GetDBConnectionV2

# from middlewares import attach_middleware
from routes import Routes
from starjet.config import settings
from db import initialize_redis_client, initialize_solr_client_v2
from utils.errors import BaseCustomException
from sanic_cors import CORS

class Starjet:
    app = None

    @classmethod
    def create_app(cls):
        if cls.app:
            return cls.app

        app = Sanic(__name__)
        CORS(app) 
        app.config.update_config(settings)
        for name in logging.root.manager.loggerDict:
            logging.getLogger(name).setLevel(settings.LOG_LEVEL)

        # app.static("/grafana.json", "./grafana.json", name="grafana")
        # configure_sentry(settings)

        def initialize_blueprint(app):
            from product.urls.platform import product_pltm_apis
            from billing.urls.platform import billing_pltm_apis
            BASE_URLS = Blueprint("main")

            # base URL for the server such as swagger, health
            base_group = Blueprint.group(BASE_URLS)
            app.blueprint(base_group)
            server_type = settings.SERVER_TYPE

            if server_type in {"local", "test"}:
                # All the server routes will be added to the blueprint with their respective URL prefix
                # Example: internal -> /internal/v1.0/...
                from routes.platform import PlatformRouters
                pltm_apis = Blueprint.group(
                    product_pltm_apis,
                    billing_pltm_apis,
                )
                app.blueprint(
                    pltm_apis
                )

            else:
                server_routes = Routes.get(server_type)
                app.blueprint(Blueprint.group(*server_routes, url_prefix=f"/{server_type}" if settings.DEBUG else None))

        async def intitialise_app(app):

            await initialize_redis_client()
            initialize_solr_client_v2()
            # await attach_middleware(app)

        if not app.blueprints:
            initialize_blueprint(app)
            asyncio.get_event_loop().run_until_complete(intitialise_app(app))

        @app.before_server_start
        async def init_databases(app, loop):
            db_conn = GetDBConnectionV2()
            db_conn.initialise_db_conn(
                loop, settings.MONGO_DATABASES, app_name=settings.POD_NAME
            )
            initialize_mongo_connection(
                {
                    "databases": settings.MONGO_DATABASES,
                    "loop": loop,
                    "app_name": settings.POD_NAME,
                }
            )

        @app.exception(RequestTimeout)
        async def timeout(request: Request, exception: RequestTimeout):
            error = {"error": "timeout", "message": "The request timed out."}
            return response.json(error, exception.status_code)

        @app.exception(NotFound)
        async def timeout(request: Request, exception: NotFound):
            error = {"error": "invalid url", "message": "The request url not found."}
            request.args["pathNotFound"] = True
            return response.json(error, exception.status_code)

        @app.exception(BaseCustomException)
        async def add_error(request, exception):
            return response.json(exception.errors, exception.status_code)

        cls.app = app
        return cls.app
