import os
import configargparse
import sys

root_dir = os.path.dirname(os.path.abspath(__file__))
default_config_files = "{0}/{1}".format(root_dir, "default.yml")

parser = configargparse.ArgParser(config_file_parser_class=configargparse.YAMLConfigFileParser,
                                  default_config_files=[default_config_files],
                                  auto_env_var_prefix="")

parser.add('--server_type', help='SERVER_TYPE')
parser.add('--resource_type', help='RESOURCE_TYPE')
parser.add('--port', help='PORT')
parser.add('--debug', help='debug')
parser.add('--mode', help='MODE')

parser.add('--redis_starjet_cache_read_write', help='REDIS_STARJET_CACHE')
parser.add('--kafka_broker_list', help='kafka_broker_list')

parser.add_argument("--mongo_starjet_read_write")

parser.add('--consumer_type', help='CONSUMER_TYPE')
parser.add('--REALM', help="REALM")
parser.add('--k8s_pod_name', help="K8S_POD_NAME")
parser.add('--k8s_node_name', help="K8S_NODE_NAME")
parser.add('--k8s_pod_namespace', help="K8S_POD_NAMESPACE")
parser.add('--METRICS_DIR', help="METRICS_DIR")
parser.add('--store_verification', help="STORE_VERIFICATION")
parser.add('--encrypt_secret', help='encrypt_secret')
parser.add('--encrypt_iv', help='encrypt_iv')

parser.add('--common_zookeeper_nodes', help='COMMON_ZOOKEEPER_NODES')
parser.add('--solr_config_mode', help='SOLR_CONFIG_MODE')
parser.add('--solr_config_url', help='SOLR_CONFIG_URL')
parser.add('--solr_config_core_or_collection', help='COLLECTION')
parser.add("--solr_config_core_or_collection_v2", help="SOLR_CONFIG_CORE_OR_COLLECTION_V2")
parser.add('--project_name', help="PROJECT_NAME")

arguments = sys.argv
argument_options = parser.parse_known_args(arguments)
docker_args = argument_options[0]
