import logging
import os
from pymongo import ReadPreference
from starjet.config.config_parser import docker_args
from utils.common import convert_str_to_bool

args = docker_args

class DockerConfig(object):
    PORT = args.port
    APP_NAME = 'starjet'
    REALM = args.REALM
    solr_v2 = True

    # @TODO: These static config params must be inherited from base common settings
    REQUEST_STATS_WINDOW = 15

    DEBUG = convert_str_to_bool(args.debug)
    MODE = args.mode

    # CRON_JOB = args.cron_job
    BASE_DIR = os.path.dirname(os.path.abspath(__file__))
    # SECRET_KEY = os.environ.get("SECRET_KEY", "51f52814-0071-11e6-a247-000ec6c2372c")

    SERVER_TYPE = args.server_type
    RESOURCE_TYPE = args.resource_type
    # CLOUD_PROVIDER = args.cloud_provider

    MONGO_DATABASES = {
        "starjet": {
            "host": args.mongo_starjet_read_write,
            "read_preference": ReadPreference.SECONDARY_PREFERRED,
        }
    }

    # MEDIA_DIR = "media"
    # FILES_DIR = "{}/{}".format(MEDIA_DIR, "files")
    # TEMP_DIR = "/tmp"

    LOG_LEVEL = logging.DEBUG
    WORKERS = 1
    # ASSETS_BUCKET_PREFIX = args.assets_bucket_prefix

    # CORS_ORIGIN = []
    # ASSETS_BUCKET_PREFIX = args.assets_bucket_prefix

    KAFKA_CONFIG = {
        "batch.num.messages": 100000,
        "linger.ms": 10,
        "acks": 1,
        "retries": 1,
        "compression.type": "lz4",
    }

    KAFKA_TOPICS = {
        "product-approval": "fynd-json-product-approval",
    }

    CACHES = {
        "starjet_cache": args.redis_starjet_cache_read_write,
    }
    KAFKA_BROKER_CONFIG = {"default": args.kafka_broker_list}

    KAFKA_CONSUMERS = {
        "common": {
            "consumer_service": "CommonConsumer",
            "topics": [
                KAFKA_TOPICS.get("product-approval"),
            ],
            "max_records": 1,
        }
    }

    KAFKA_CONSUMER_CONFIG = {
        "CommonConsumer": {
            "bootstrap_servers": KAFKA_BROKER_CONFIG.get("default"),
            "auto_offset_reset": "latest",
            "group_id": "starjet-ps-group-1",
            "enable_auto_commit": False,
        },
    }

    CONSUMER_TYPE = args.consumer_type

    MONGO_HOST = "127.0.0.1"
    REDIS_HOST = "127.0.0.1"

    default_youtube_thumbnail_key = (
        "media/video/item/thumbnails/default/youtube-default.jpg"
    )

    # GENERATE_PROMETHEUS_METRICS = args.GENERATE_PROMETHEUS_METRICS

    # METRICS_DIR = args.metrics_dir
    # PLATFORM_COMMON_MAIN_URL = args.platform_common_main_url
    # AWS_REGION_NAME = args.aws_region
    # AWS_SECRET_KEY = args.aws_secret_access_key
    # AWS_ACCESS_KEY = args.aws_access_key_id

    POD_NAME = args.k8s_pod_name
    NODE_NAME = args.k8s_node_name
    POD_NAMESPACE = args.k8s_pod_namespace
    PROJECT_NAME = args.project_name

    # if args.solr_config_mode == "solrcloud":
    #     SOLR_CONFIG_V2 = {
    #         "mode": args.solr_config_mode,
    #         "collection": args.solr_config_core_or_collection_v2,
    #         "zookeeper_nodes": args.common_zookeeper_nodes_v2,
    #     }
    # else:
        # SOLR_CONFIG_V2 = {
        #     "mode": args.solr_config_mode,
        #     "core": args.solr_config_core_or_collection_v2,
        #     "uri": args.solr_config_url,
        # }