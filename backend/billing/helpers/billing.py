from databases.mongo.models.product import Product as ProductModel
from databases.mongo.models.billing import Billing as BillingModel
from bson import ObjectId

async def save_bill(data, company_id, _user):
    uids = list(map(int, data.get("products").keys()))
    query = {"uid":{"$in":uids}, "company_id":data.get("company_id")}
    products = ProductModel.connect().find(query)
    order_bill = await calculate_and_save_billing(products, company_id, _user, data)
    return order_bill
    
async def calculate_and_save_billing(items, company_id, _user, data, gst_rate=0.18):

    total_cost = 0.0
    gst_amount = 0.0
    order_billing = {"order":[]}

    async for item in items:
        item_quantity = data.get('products').get(str(item.get("uid")))
        discounted_price = item.get("discounted_price", 0.0)  # Get the discount percentage
        total_cost += discounted_price * item_quantity
        order_billing["order"].append({
            "product_uid": item.get("uid"),
            "product_quantity": item_quantity 
        })
        
    gst_amount = total_cost * gst_rate
    total_cost += gst_amount
    order_billing["total_amount"] = total_cost
    order_billing["user_name"] = _user.get("username")
    order_billing["company_id"] = company_id

    order_bill = await BillingModel.insert_and_get_one_document(order_billing)
    order_bill["_id"] = str(order_bill["_id"])
    return order_bill

async def get_billing(billing_id):
    billing_item = []
    query = get_query_pipeline(billing_id)
    bill = BillingModel.connect().aggregate(query)
    async for bill_item in bill:
        billing_item.append(bill_item)
    for item in billing_item[0].get("productInfo"):
        item["total_price"] = item["price"]*item["product_quantity"]
    return billing_item[0]

def get_query_pipeline(billing_id):
    pipeline = [
        {
        "$match": {
        "_id": ObjectId(billing_id)
        }
    },
    {
        "$unwind": "$order"
    },
    {
        "$lookup": {
        "from": "product",
        "localField": "order.product_uid",
        "foreignField": "uid",
        "as": "productInfo"
        }
    },
    {
        "$unwind": "$productInfo"
    },
    {
        "$group": {
        "_id": "$_id",
        "total_amount": { "$first": "$total_amount" },
        "user_name": { "$first": "$user_name" },
        "company_id": { "$first": "$company_id" },
        "productInfo": {
            "$push": {
            "_id": "$productInfo._id",
            "name": "$productInfo.name",
            "uid": "$productInfo.uid",
            "company_id": "$productInfo.company_id",
            "price": "$productInfo.price",
            "discount": "$productInfo.discount",
            "discounted_price": "$productInfo.discounted_price",
            "product_quantity": "$order.product_quantity"
            }
        }
        }
    }
    ]
    return pipeline
    