from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet
from io import BytesIO
from sanic import response
from sanic.response import file

async def generate_pdf(invoice_data):
    # Create a PDF buffer, using BytesIO
    buffer = BytesIO()

    # Create the PDF object, using the buffer as its "file"
    doc = SimpleDocTemplate(buffer, pagesize=letter)

    # Create a list to hold the invoice content
    elements = []

    # Styles
    styles = getSampleStyleSheet()
    style_table = TableStyle([
        ('BACKGROUND', (0, 0), (-1, 0), colors.grey),
        ('TEXTCOLOR', (0, 0), (-1, 0), colors.whitesmoke),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
        ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
        ('BOTTOMPADDING', (0, 0), (-1, 0), 12),
        ('BACKGROUND', (0, 1), (-1, -1), colors.beige),
        ('GRID', (0, 0), (-1, -1), 1, colors.black)
    ])

    # Invoice Header
    elements.append(Paragraph("Invoice", styles['Heading1']))

    # Invoice Information
    elements.append(Paragraph("Invoice ID: {}".format(invoice_data['_id']), styles['Normal']))
    elements.append(Paragraph("User Name: {}".format(invoice_data['user_name']), styles['Normal']))
    elements.append(Paragraph("Company ID: {}".format(invoice_data['company_id']), styles['Normal']))

    # Create a table for product information
    product_data = []
    product_data.append(["Product Name", "Price", "Quantity", "Discounted Price", "Total"])
    total_amount = 0

    for product in invoice_data['productInfo']:
        product_name = product['name']
        price = product['price']
        quantity = product['product_quantity']
        discounted_price = product['discounted_price']
        total = quantity * discounted_price
        product_data.append([product_name, price, quantity, discounted_price, total])

    total_amount = invoice_data['total_amount']
    product_table = Table(product_data)
    product_table.setStyle(style_table)

    elements.append(product_table)

    # Added GST
    elements.append(Paragraph("Added GST: 18 %", styles['Normal']))

    # Total Amount
    elements.append(Paragraph("Total Amount: RS.{:.2f}".format(total_amount), styles['Normal']))

    # Build the PDF document
    doc.build(elements)

    # Get the value of the BytesIO buffer
    pdf_data = buffer.getvalue()
    buffer.close()

    # # Save the PDF to a file (optional)
    # with open("invoice.pdf", "wb") as pdf_file:
    #     pdf_file.write(pdf_data)

    filename = "invoice_{}.pdf".format(invoice_data['_id'])  # Use a shorter filename with the invoice ID
    response_pdf = response.raw(pdf_data, content_type="application/pdf")
    response_pdf.headers["Content-Disposition"] = 'attachment; filename=invoice.pdf'
    return response_pdf

    # Now, pdf_data contains the PDF content that you can use or serve as needed.
