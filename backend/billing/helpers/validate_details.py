from utils.common import validate_x_user_data


async def validate_billing_request(request, company_id):

    _user = validate_x_user_data(request)
    request_data = request.json or {}
    request_data["company_id"] = company_id
    return request_data, _user