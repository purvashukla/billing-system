from sanic import Blueprint

from billing.views import GenerateBilling, GetBilling, DownloadBill

billing_pltm_apis = Blueprint(
    'pltm_billing', url_prefix='/v1.0/company/<company_id:int>/')

# list, create
billing_pltm_apis.add_route(GenerateBilling.as_view(), 'billing/')

billing_pltm_apis.add_route(GetBilling.as_view(), 'billing/<id>')

billing_pltm_apis.add_route(DownloadBill.as_view(), 'download-billing/<id>')

# retrive, update, delete
# product_pltm_apis_v1_0.add_route(
#     ProductRetriveUpdateDelete.as_view(), 'products/<item_id:int>/')

# # validate uniqueness
# product_pltm_apis_v1_0.add_route(ProductValidationView.as_view(), 'products/validation/')