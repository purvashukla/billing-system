from sanic.views import HTTPMethodView
from sanic import response
from billing.helpers.validate_details import validate_billing_request
from billing.helpers.billing import save_bill, get_billing
from billing.helpers.pdf_generation import generate_pdf
import ujson
from utils.common import UjsonDump

class GenerateBilling(HTTPMethodView):

    async def post(self, request, company_id):

        request_data, _user = await validate_billing_request(request, company_id)
        response_data = await save_bill(request_data, company_id, _user)
        return response.json({"data": response_data})

class GetBilling(HTTPMethodView):

    async def get(self, request, company_id, id):

        # request_data, _user = await validate_billing_request(request, company_id)
        response_data = await get_billing(id)
        return response.json({"data": response_data}, dumps=UjsonDump.dumps)

    # async def get(self, request, company_id):

    #     data = request.args
    #     data["company_id"] = company_id
    #     products = await get_products(data)
    #     resp = {"items": products}
    #     return response.json(resp)


class DownloadBill(HTTPMethodView):

    async def get(self, request, company_id, id):

        billing_data = await get_billing(id)
        response = await generate_pdf(billing_data)
        return response
