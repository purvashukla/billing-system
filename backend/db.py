# from pysolrclient.zookeeper import ZooKeeper
# from pysolrclient.solr import Solr
# from pysolrclient.solr_cloud import SolrCloud

from redis_utils import ConnectRedis
from starjet.config import settings


solr_suggester_core = "autocomplete_v3"


async def initialize_redis_client():
    ConnectRedis(settings.CACHES)


def get_solr_client(solr_config):
    solr_client_suggester = None
    solr_client = None
    # if solr_config["mode"] == "solrcloud":
    #     zookeeper = ZooKeeper(solr_config["zookeeper_nodes"])
    #     solr_client = SolrCloud(zookeeper, solr_config["collection"])
    #     solr_client_suggester = SolrCloud(zookeeper, solr_suggester_core)
    # else:
    #     solr_client = Solr(solr_config["uri"], core=solr_config["core"])
    #     solr_client_suggester = Solr(solr_config["uri"], core=solr_config["core"])

    return solr_client, solr_client_suggester


def initialize_solr_client_v2():
    pass
    # settings.solr_client_v2, settings.solr_client_suggester = get_solr_client(settings.SOLR_CONFIG_V2)
